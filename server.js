var express = require("express"); //using the express web framework

var reviewsController = require("./controllers/reviewsController"); // set reviewController to the reviewController class
var restaurantController = require("./controllers/restaurantController"); // set restaurantController to the restaurantController class
var userController = require("./controllers/userController");
var favouriteController = require("./controllers/favouriteController");
var app = express(); // set variable app to be an instance of express framework. From now on, app is the express
var cors = require("cors");

app.use(cors());
app.use(express.static("./public")); //static files are to be served from the public folder - for eg. html, images, css
app.use(express.json()); // json() is a method inbuilt in express to recognize the incoming Request Object from the web client as a JSON Object.
// In time to come we will need to accept new or edited comments from user

app.route("/restaurant").get(restaurantController.getAllRestaurants); // activate the getAllRestaurants method if the route is GET(method) /Restaurants

app.route("/reviews").get(reviewsController.getAllReviews); // activate the getAllReviews method if the route is GET(method) /reviews
app.route("/reviews").post(reviewsController.addReview); // activate the addReview method if the route is POST(method) /reviews
app.route("/reviews/:_idReviews").put(reviewsController.updateReview); // activate the updateComments method if the route is PUT(method) /comments/:id
app.route("/reviews/:_idReviews").delete(reviewsController.deleteReview); // activate the deleteComment method if the route is DELETE(method) /comments/:id
app
  .route("/getReviewCountIndivRestaurant/:restaurant_id")
  .get(reviewsController.getReviewCountIndivRestaurant);
app
  .route("/getAverageRating/:restaurant_id")
  .get(reviewsController.getAverageRating); // get the average rating of a specific restaurant based on their ID
app.route("/getReviewsbyUser/:user_id").get(reviewsController.getReviewsbyUser);

app.route("/users").get(userController.getAllUser);
app.route("/users").post(userController.addUser);
app.route("/users").put(userController.updateUser);
app.route("/users/:_idUser").delete(userController.deleteUser);
app.route("/users/:user_postalCode").get(userController.getSuggestedAddress); //get User's Suggested Address from their postal code
app.route("/loginUser").post(userController.loginUser);

app.route("/favourite").get(favouriteController.getAllFavourites);
app.route("/getFavbyID/:user_idF").get(favouriteController.getFavbyID);

app.listen(4200, "127.0.0.1"); // start the nodejs to be listening for incoming request @ port 8080
console.log("web server running @ http://127.0.0.1:4200"); // output to console
