//This function is to call the movies api and get all the movies
//that is showing in Shaw Theatres for Showing Now and Coming Soon
function getRestaurantData() {
  var request = new XMLHttpRequest();
  request.open("GET", restaurant_url, true);
  //This function will be called when data returns from the web api
  request.onload = function () {
    //get all the movies records into our movie array
    restaurant_array = JSON.parse(request.responseText);
    //Fetch the comments as well
    //  fetchComments();
    console.log(restaurant_array);

    let name = localStorage.getItem("username");
    console.log("name", name);
    if (name) {
      console.log("name", name);
      document.getElementById("username").style.display = "block";
      document.getElementById("username").innerHTML = name;
    }

    // output to console
    //call the function so as to display all movies tiles for "Now Showing"
    displayRestaurant();
  };

  //This command starts the calling of the movies web api
  request.send();
}

function displayRestaurant() {
  var table = document.getElementById("restaurantTable");
  var restaurantCount = 0;
  var message = "";

  table.innerHTML = "";
  totalRestaurant = restaurant_array.length;
  for (var count = 0; count < 1; count++) {
    if (restaurant_array[count]) {
      var thumbnail = restaurant_array[count].rest_mainphoto;
      var title = restaurant_array[count].rest_name;
      var geographic = restaurant_array[count].rest_geographic;
      var cuisine = restaurant_array[count].rest_cuisine;
      var id = restaurant_array[count]._idRestaurant;
      var rating = restaurant_array[count].avgRating;
      var reviewCount = restaurant_array[count].reviewCount;
      var cell =
        '<div style="width: 60%;height: 320px;display: flex;margin-right: 20px;margin-bottom: 20px;">\
<img src="' +
        thumbnail +
        '" alt="Girl in a jacket" width="50%" height="320"><div style="margin-left: 10px;display: flex;flex-direction: column;justify-content: center;">\
    <p style="font-size: 22px;color: #1e276b;">' +
        title +
        '</p><p style="font-size: 22px;color: #1e276b;">Location: ' +
        geographic +
        '</p>\
    <p style="font-size: 22px;color: #1e276b;">Type:' +
        cuisine +
        '</p>\
    <p style="font-size: 22px;">' +
        title +
        "</p>\
<div id=r" +
        id +
        ' style="display: flex;">\
</div><a style="font-size: 30px;" href="ResturantReview.html?id:' +
        id +
        '">\
<button style="height: 40px;border-width:0px;background-color: #1e276b;;margin-top: 5px;width: 100px;" type="button" class="btn btn-primary">Read More</button>\
</a>\
</div>\
</div>';
      //       var cell =
      //         '<div class="card col-md-3" ><img class="card-img-top" src="' +
      //         thumbnail +
      //         '" alt="Card image cap">\
      //                         <div class="card-body"><i class="far fa-comment fa-lg" style="float:left;cursor:pointer" data-toggle="modal" data-target="#commentModal" item="' +
      //         count +
      //         '" onClick="showMovieComments(this)"></i>\
      //                         <h5 style="padding-left:30px;cursor:pointer" data-toggle="modal" data-target="#restaurantModal" class="card-title" item="' +
      //         count +
      //         '" onClick="showRestaurantDetails(this)">' +
      //         title +
      //         "</h5></div>\
      // </div>";
      table.insertAdjacentHTML("beforeend", cell);

      let ids = "r" + id;
      console.log("id parseINt", parseInt(id));
      console.log("reviewcount", reviewCount);

      switch (parseInt(rating)) {
        case 0:
          let p1 =
            '<p style="font-size: 16px;color: #2a3273;">Ratings:</p>\
  <div style="margin-left: 5px;margin-right: 5px;">\
          <span  class="fa fa-star" ></span>\
<span class="fa fa-star" ></span>\
<span class="fa fa-star checked"></span>\
<span class="fa fa-star"></span>\
<span class="fa fa-star"></span>\
</div>\
</div>\
</div>';

          document.getElementById(ids).innerHTML = p1;
          // code block
          break;
        case 1:
          let p2 =
            '<p style="font-size: 16px;color: #2a3273;">Ratings:</p>\
  <div style="margin-left: 5px;margin-right: 5px;">\
          <span  class="fa fa-star"   style="color: yellow;" ></span>\
<span class="fa fa-star" ></span>\
<span class="fa fa-star checked"></span>\
<span class="fa fa-star"></span>\
<span class="fa fa-star"></span>\
</div>\
</div>\
</div>';
          document.getElementById(ids).innerHTML = p2;
          // code block
          break;
        case 2:
          let p3 =
            '<p style="font-size: 16px;color: #2a3273;">Ratings:</p>\
  <div style="margin-left: 5px;margin-right: 5px;">\
          <span  class="fa fa-star"   style="color: yellow;" ></span>\
<span class="fa fa-star"   style="color: yellow;" ></span>\
<span class="fa fa-star checked"></span>\
<span class="fa fa-star"></span>\
<span class="fa fa-star"></span>\
</div>\
</div>\
</div>';
          document.getElementById(ids).innerHTML = p3;
          // code block
          break;
        case 3:
          let p4 =
            '<p style="font-size: 16px;color: #2a3273;">Ratings:</p>\
  <div style="margin-left: 5px;margin-right: 5px;">\
          <span  class="fa fa-star"   style="color: yellow;" ></span>\
<span class="fa fa-star"   style="color: yellow;" ></span>\
<span class="fa fa-star checked "   style="color: yellow;"></span>\
<span class="fa fa-star"></span>\
<span class="fa fa-star"></span>\
</div>\
</div>\
</div>';
          document.getElementById(ids).innerHTML = p4;
          // code block
          break;
        case 4:
          let p5 =
            '<p style="font-size: 16px;color: #2a3273;">Ratings:</p>\
  <div style="margin-left: 5px;margin-right: 5px;">\
          <span  class="fa fa-star"   style="color: yellow;" ></span>\
<span class="fa fa-star"   style="color: yellow;" ></span>\
<span class="fa fa-star checked"   style="color: yellow;"></span>\
<span class="fa fa-star"   style="color: yellow;"></span>\
<span class="fa fa-star"></span>\
</div>\
</div>\
</div>';
          document.getElementById(ids).innerHTML = p5;
          // code block
          break;
        case 5:
          let p6 =
            '<p style="font-size: 16px;color: #2a3273;">Ratings:</p>\
  <div style="margin-left: 5px;margin-right: 5px;">\
          <span  class="fa fa-star"   style="color: yellow;" ></span>\
<span class="fa fa-star"   style="color: yellow;" ></span>\
<span class="fa fa-star checked"   style="color: yellow;"></span>\
<span class="fa fa-star"   style="color: yellow;"></span>\
<span class="fa fa-star"   style="color: yellow;"></span>\
</div>\
</div>\
</div>';
          document.getElementById(ids).innerHTML = p6;
          // code block
          break;
        default:
        // code block
      }

      let p7 =
        '<p style="font-size: 17px;color: #1e276b;">(' + reviewCount + ")</p>";
      document.getElementById(ids).innerHTML =
        document.getElementById(ids).innerHTML + p7;

      restaurantCount++;
    }
  }

  message = restaurantCount + category;
  document.getElementById("summary").textContent = message;
  document.getElementById("parent").textContent = "";
}

//This function is to display the "Now Showing" movies
/*function listNowShowingMovies() {
    category = "Now Showing";
    displayRestaurant(category);
    document.getElementById("nowMenu").classList.add("active");
    document.getElementById("comingMenu").classList.remove("active");
    document.getElementById("aboutMenu").classList.remove("active");
}*/

//This function is to display the "Coming Soon" movies
/*function listComingMovies() {
    category = "Coming Soon";
    displayRestaurant(category);
    document.getElementById("nowMenu").classList.remove("active");
    document.getElementById("comingMenu").classList.add("active");
    document.getElementById("aboutMenu").classList.remove("active");
}*/

//This function is to display the individual movies details
//whenever the user clicks on "See More"
function showRestaurantDetails(element) {
  var item = element.getAttribute("item");
  currentIndex = item;
  document.getElementById("Restaurant Name").textContent =
    restaurant_array[item].rest_name;
  document.getElementById("Geographics").textContent =
    restaurant_array[item].rest_geographic;
  document.getElementById("Cuisine").textContent =
    restaurant_array[item].rest_cuisine;
  document.getElementById("Description").textContent =
    restaurant_array[item].rest_description;
  document.getElementById("Opening Hours").textContent =
    restaurant_array[item].rest_Ohours;
  document.getElementById("Address").textContent =
    restaurant_array[item].rest_address;
  document.getElementById("Telephone").textContent =
    restaurant_array[item].rest_tel;
  document.getElementById("Website").textContent =
    restaurant_array[item].rest_website;
}

//This function opens a new window/tab and loads the
//particular movie in the cinema website
function buyTicket() {
  window.open(movie_array[currentIndex].buy, "_blank");
}
