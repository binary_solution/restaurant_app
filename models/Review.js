"use strict";

class review {
    constructor(_idReviews, restaurant_id, user_id, rev_restName, review, rev_rating, rev_date, ) {
        this._idReviews = _idReviews;
        this.restaurant_id = restaurant_id;
        this.user_id = user_id;
		this.rev_restName = rev_restName;
        this.review = review;
        this.rev_rating = rev_rating;
        this.rev_date = rev_date;
    }
    get_idReviews() {
        return this._idReviews;
    }

    getRestaurantId() {
        return this.restaurant_id;
    }

    getUserId() {
        return this.user_id;
    }
	
	getRestaurant() {
        return this.rev_restName;
    }
	
	getReview() {
		return this.review;
	}

	getReviewRating() {
		return this.rev_rating;
	}

	getReviewdPosted() {
		return this.rev_date;
	}


    set_idReviews (_idReviews){
        this._idReviews = _idReviews;
    }
    setRestaurantId(restaurant_id) {
        this.restaurant_id = restaurant_id;
    }

    setRestaurant(rev_restName) {
        this.rev_restName = rev_restName;
    }
	
    setReview(review) {
        this.review = review;
    }

    setReviewdRatings(rev_rating) {
        this.rev_rating = rev_rating;
    }

    setReviewdPosted(rev_date) {
        this.rev_date = rev_date;
    }	


	
	
}

module.exports = review;