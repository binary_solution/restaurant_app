"use strict";

var db = require("../db-connection");

class UserDB {
  getAllUser(callback) {
    var sql =
      "SELECT user_Uname, user_fName, user_lName, user_phoneNo, user_email, user_img FROM mydb.user";
    db.query(sql, callback);
  }

  addUser(
    username,
    password,
    firstname,
    lastname,
    gender,
    age,
    address,
    postalcode,
    phoneno,
    email,
    userimage,
    callback
  ) {
    var sql =
      "INSERT INTO user (user_Uname, user_passW, user_fName, user_lName, user_gender, user_age, user_address, user_postalCode, user_phoneNo, user_email, user_img) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    db.query(
      sql,
      [
        username,
        password,
        firstname,
        lastname,
        gender,
        age,
        address,
        postalcode,
        phoneno,
        email,
        userimage,
      ],
      callback
    );
  }

  loginUser(username, callback) {
    var sql = "SELECT * FROM mydb.user WHERE user_Uname = ?";
    db.query(sql, [username], callback);
  }

  updateUser(
    username,
    firstname,
    lastname,
    gender,
    age,
    address,
    postalcode,
    phoneno,
    email,
    userimage,
    callback
  ) {
    var sql =
      "UPDATE mydb.user SET user_fName = ? , user_lName = ?, user_gender = ?,user_age = ?, user_address = ?, user_postalCode = ?, user_phoneNo = ?, user_email = ?,  user_img = ? WHERE user_Uname = ?";
    db.query(
      sql,
      [
        firstname,
        lastname,
        gender,
        age,
        address,
        postalcode,
        phoneno,
        email,
        userimage,
        username,
      ],
      callback
    );
  }

  deleteUser(_idUser, callback) {
    var sql = "DELETE FROM user WHERE _idUser = ?";
    db.query(sql, _idUser, callback);
  }

  getSuggestedAddress(user_postalCode, callback) {
    var sql = "SELECT user_address FROM mydb.user WHERE user_postalCode = ?";
    return db.query(sql, user_postalCode, callback);
  }
}

/*addUser(user, callback){
        var sql = "INSERT INTO user (user_Uname, user_passW, user_fName, user_lName, user_gender, user_age, user_address, user_postalCode, user_phoneNo, user_email, user_img) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        db.query(sql,  [user.getUsername(),
                        user.getPassword(),
                        user.getFirstname(),
                        user.getLastname(), 
                        user.getGender(), 
                        user.getAge(),
                        user.getAddress(),
                        user.getPostalcode(),
                        user.getPhoneNo(),
                        user.getEmail(),
                        user.getImage()], 
                        callback);*/

module.exports = UserDB;
