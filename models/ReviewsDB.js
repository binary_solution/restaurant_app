"use strict";

var db = require("../db-connection");

class ReviewsDB {
  getAllReviews(callback) {
    var sql =
      "SELECT reviews.*,user_Uname from mydb.reviews inner join user on user._idUser=reviews.user_id order by reviews._idReviews desc";
    db.query(sql, callback);
  }

  addReview(comment, callback) {
    var sql =
      "INSERT INTO reviews (restaurant_id, user_id, rev_restName, review, rev_rating, rev_date) VALUES (?, ?, ?, ?, ?, ?)";
    db.query(
      sql,
      [
        comment.getRestaurantId(),
        comment.getUserId(),
        comment.getRestaurant().trim(),
        comment.getReview(),
        comment.getReviewRating(),
        comment.getReviewdPosted(),
      ],
      callback
    );
  }

  updateReview(comment, callback) {
    var sql =
      "UPDATE mydb.reviews SET restaurant_id = ?,user_id = ?, rev_restName = ? , review = ?, rev_rating = ?,rev_date = ? WHERE _idReviews = ?";
    return db.query(
      sql,
      [
        comment.getRestaurantId(),
        comment.getUserId(),
        comment.getRestaurant(),
        comment.getReview(),
        comment.getReviewRating(),
        comment.getReviewdPosted(),
        comment.get_idReviews(),
      ],
      callback
    );
  }

  deleteReview(_idReviews, callback) {
    var sql = "DELETE FROM reviews WHERE _idReviews = ?";
    db.query(sql, _idReviews, callback);
  }

  getReviewCountIndivRestaurant(restaurant_id, callback) {
    var sql =
      "SELECT COUNT(review) as reviewCount FROM mydb.reviews WHERE restaurant_id = ?";
    return db.query(sql, restaurant_id, callback);
  }

  getAverageRating(restaurant_id, callback) {
    var sql =
      "SELECT AVG (rev_rating) as avgRating FROM mydb.reviews WHERE restaurant_id = ?";
    return db.query(sql, restaurant_id, callback);
  }

  getReviewsbyUser(user_id, callback) {
    var sql =
      "SELECT reviews.user_id, user.user_Uname, reviews.review, reviews.rev_restName FROM mydb.reviews INNER JOIN mydb.user ON reviews.user_id = user._idUser WHERE user_id = ? order by reviews._idReviews";
    return db.query(sql, user_id, callback);
  }
}

module.exports = ReviewsDB;
