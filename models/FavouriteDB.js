"use strict";

var db = require('../db-connection');

class FavouriteDB{
    getAllFavourites(callback){
        var sql = "SELECT * from mydb.favourites";
        db.query(sql, callback);
    }
    
    getFavbyID(user_idF, callback){
        var sql = "SELECT user.user_Uname, restaurant.rest_name, restaurant.rest_geographic, restaurant.rest_description FROM mydb.favourites JOIN mydb.restaurant ON favourites.restaurant_idF = restaurant._idRestaurant JOIN mydb.user ON favourites.user_idF = user._idUser WHERE user_idF = ?";
        db.query(sql, [user_idF], callback);
    }
    
}
    




module.exports = FavouriteDB;