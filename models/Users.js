"use strict";

class User {
    constructor(_idUser, user_Uname, user_passW, user_fName, user_lName, user_gender, user_age, user_address, user_postalCode, user_phoneNo, user_email, user_img) {
        this._idUser = _idUser;
        this.user_Uname = user_Uname;
        this.user_passW = user_passW;
        this.user_fName = user_fName;
        this.user_lName = user_lName;
        this.user_gender = user_gender;
        this.user_age = user_age;
        this.user_address = user_address;
        this.user_postalCode = user_postalCode;
        this.user_phoneNo = user_phoneNo;
        this.user_email = user_email;
        this.user_img = user_img;
    }

    getUserID() {
        return this._idUser;
    }

    getUsername() {
        return this.user_Uname;
    }

    getPassword() {
        return this.user_passW;
    }

    getFirstname() {
        return this.user_fName;
    }

    getLastname(){
        return this.user_lName;
    }

    getGender(){
        return this.user_gender;
    }

    getAge(){
        return this.user_age;
    }

    getAddress(){
        return this.user_age;
    }

    getPostalcode(){
        return this.user_postalCode;
    }

    getPhoneNo(){
        return this.user_phoneNo;
    }

    getEmail(){
        return this.user_email;
    }

    getImage(){
        return this.user_img;
    }

    
//Set
    setUserID(_idUser) {
        this._idUser = _idUser;
    }

    setUsername(user_Uname) {
        this.user_Uname = user_Uname;
    }

    setPassword(user_passW) {
        this.user_passW = user_passW;
    }

    setFirstname(user_fName) {
        this.user_fName = user_fName;
    }

    setLastname(user_lName) {
        this.user_lName = user_lName;
    }

    setLastname(user_lName) {
        this.user_lName = user_lName;
    }

    setGender(user_gender) {
        this.user_gender = user_gender;
    }

    setAge(user_age) {
        this.user_age = user_age;
    }

    setAddress(user_address) {
        this.user_address = user_address;
    }

    setPostalcode(user_postalCode) {
        this.user_postalCode = user_postalCode;
    }

    setPhoneNo(user_phoneNo) {
        this.user_phoneNo = user_phoneNo;
    }

    setEmail(user_email) {
        this.user_email = user_email;
    }

    setImage(user_img) {
        this.user_img = user_img;
    }

}

module.exports = User;