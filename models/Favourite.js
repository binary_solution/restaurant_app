"use strict";

class favourites {
    constructor(_idFavourites, user_idF, restaurant_idF, restaurant_name) {
        this._idFavourites = _idFavourites;
        this.user_idF = user_idF;
        this.restaurant_idF = restaurant_idF;
        this.restaurant_name = restaurant_name;
    }

    getFavouriteID() {
        return this._idFavourites;
    }

    getUserFavID() {
        return this.user_idF;
    }

    getRestaurantFavID() {
        return this.restaurant_idF;
    }

    getRestaurantFav(){
        return this.restaurant_name;
    }
	


    setFavouriteID(_idFavourites){
        this._idFavourites = _idFavourites;
    }
    setUserFavID(user_idF) {
        this.user_idF = user_idF;
    }

    setRestaurantFavID(restaurant_idF) {
        this.restaurant_idF = restaurant_idF;
    }

    setRestaurantFav(){
        return this.restaurant_name;
    }

	
	
}

module.exports = favourites;