-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: mydb
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `_idUser` int NOT NULL AUTO_INCREMENT,
  `user_Uname` varchar(40) NOT NULL,
  `user_passW` varchar(150) DEFAULT NULL,
  `user_fName` varchar(100) NOT NULL,
  `user_lName` varchar(100) NOT NULL,
  `user_gender` varchar(8) NOT NULL,
  `user_age` varchar(10) NOT NULL,
  `user_address` varchar(255) NOT NULL,
  `user_postalCode` varchar(10) NOT NULL,
  `user_phoneNo` varchar(8) NOT NULL,
  `user_email` varchar(250) NOT NULL,
  `user_img` varchar(1024) NOT NULL,
  PRIMARY KEY (`_idUser`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Jo_de_na','Jodena001','Jodena','Tham','female','20','Blk 123 Simei Street 4 #02-123','520123','97294221','Jodenatham@gmail.com','Jodena.png'),(2,'fahira_S','Fahira001','Fahira','Sulthan','female','18','Blk 345 Little India Street 4 #03-123','908345','91236789','Fahira_s@gmail.com','Fahira.png'),(3,'Shermaine_t','Shermaine001','Shermaine','Tan','female','19','Blk 129 Tampines Ave 1 Street 1 #03-129','550129','96302468','Shermaine_T@gmail.com','Shermaine.png'),(10,'Ed_na','$2b$10$wS93CyXfXrkCEQoEZ7V2Oe5YhcyrrikwDH7xgONwoR/pInUR/i3r.','Edna','Chin','female','36','Blk 139 Potong Pasir Ave 1 Street 1 #03-139','350139','96302468','ednaChing@gmail.com','Edna.png'),(11,'Jo_shua','$2b$10$BqfHL1DAmMzV0gzCueE1deUNX7XVDaYWa6EzrYA17liv7v21bWzA2','Joshua','Tham','male','36','Blk 139 Potong Pasir Ave 1 Street 1 #03-139','350139','96305568','joshua_t@gmail.com','Joshua.png');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-01-31 19:09:26
