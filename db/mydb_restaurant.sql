-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: mydb
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `restaurant`
--

DROP TABLE IF EXISTS `restaurant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `restaurant` (
  `_idRestaurant` int NOT NULL AUTO_INCREMENT,
  `rest_name` varchar(100) NOT NULL,
  `rest_geographic` varchar(45) NOT NULL,
  `rest_cuisine` varchar(45) NOT NULL,
  `rest_description` text NOT NULL,
  `rest_Ohours` varchar(45) NOT NULL,
  `rest_Odays` varchar(45) NOT NULL,
  `rest_address` varchar(255) NOT NULL,
  `rest_tel` int NOT NULL,
  `rest_website` varchar(2048) NOT NULL,
  `rest_mainphoto` varchar(1024) NOT NULL,
  `rest_img1` varchar(1024) DEFAULT NULL,
  `rest_img2` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`_idRestaurant`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `restaurant`
--

LOCK TABLES `restaurant` WRITE;
/*!40000 ALTER TABLE `restaurant` DISABLE KEYS */;
INSERT INTO `restaurant` VALUES (1,'HaiDiLao','East','Chinese','This is the description','10.a.m to 9.p.m','Monday - Saturday','Tampines Street 44, #01-76 Block 462, Singapore 520462',61234567,'www.HaiDiLao.com','/images/Restaurant_Logos/HaiDiLao.jpg',NULL,NULL),(2,'Domino\'s Pizza','West','Western','This is the pizza description','10.a.m to 9.p.m','Monday - Saturday','Tampines Street 44, #01-76 Block 462, Singapore 520462',63214567,'www.DominosPizza.com','/images/Restaurant_Logos/Dominos_Logo.png',NULL,NULL),(3,'Ajisen Ramen','Central','Japanese','Ajisen Ramen is a Japan-based chain of fast food restaurants selling Japanese ramen noodle soup dishes. The company\'s logo, featuring artwork of a little girl named Chii-chan, can be found on their stores and products','11.30a.m to 10.p.m','Monday - Sunday','Bugis Junction, 200 Victoria St, Bugis Junction, #01-01, Singapore 188021',63339512,'https://www.ajisen.com.sg/','/images/Restaurant_Logos/Ajisen_Ramen.png',NULL,NULL),(4,'McDonald\'s','North','Western','McDonald\'s is an American fast food company, founded in 1940 as a restaurant operated by Richard and Maurice McDonald, in San Bernardino, California, United States.','11.30a.m to 10.p.m','Monday - Sunday','930 Yishun Avenue 2, #01-12/13, Northpoint City North Wing, Singapore 769098',67546043,'https://www.mcdelivery.com.sg/sg/home.html?ds_rl=1248535&ds_rl=1248535&gclid=Cj0KCQiArt6PBhCoARIsAMF5wagxgB_hlgY5pKenF_Fh1v-LnxKQpuOAV5mOWx8Wf15kAv1UzbZbL3kaAt1MEALw_wcB&gclsrc=aw.ds','/images/Restaurant_Logos/MacDonald\'s.png',NULL,NULL),(5,'Sanook Kitchen','South','Thai','A destination for adventure seeking Thai food lovers, Sanook Kitchen whips up a wide variety of authentic, unpretentious Thai meals at its best. With specially selected Thai spices and ingredients as its hallmark of authenticity, the taste of originality will leave you satisfied and wanting more. Serving up a classic range of appetisers, soup, curries to seafood and more at wallet-friendly prices, every visit will guarantee a whole new fun experience.','11.30a.m to 10.p.m','Monday - Sunday',' Temasek Boulevard #B1-134 Suntec City Mall, 038983',62612097,'https://sanookkitchen.com.sg/','/images/Restaurant_Logos/Sanook_Kitchen.jpg',NULL,NULL);
/*!40000 ALTER TABLE `restaurant` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-01-31 19:09:25
