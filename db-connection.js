var mysql = require("mysql");

var connection = mysql.createConnection({
  host: "localhost",
  port: "3306",
  user: "root",
  password: "root",
  database: "mydb",
});

connection.connect((err) => {
  // test out connetion and console.log error if there is one
  if (err) throw err;
  console.log("Connected To DB");
});

module.exports = connection;
