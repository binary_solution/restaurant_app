"use strict";
const RestaurantDB = require("../models/RestaurantDB");
const ReviewsDB = require("../models/ReviewsDB");

var restaurantDB = new RestaurantDB();
var reviewsDB = new ReviewsDB();

function getAllRestaurants(request, respond) {
  restaurantDB.getAllRestaurants(async function (error, result) {
    if (error) {
      respond.json(error);
    } else {
      //   console.log("before", result);
      const promises = result.map(async (element) => {
        try {
          const data = await getCount(element._idRestaurant);
          element.reviewCount = data.reviewCount;
        } catch (error) {
          respond.json(error);
        }
      });
      await Promise.all(promises);
      const promises1 = result.map(async (element) => {
        try {
          const data = await getAvgRating(element._idRestaurant);
          element.avgRating = data.avgRating;
        } catch (error) {
          respond.json(error);
        }
      });
      await Promise.all(promises1);
      respond.json(result);
    }
  });
}

function getCount(id) {
  return new Promise((resolve, reject) => {
    reviewsDB.getReviewCountIndivRestaurant(id, function (error, result) {
      if (error) {
        reject(error);
      } else {
        resolve(result[0]);
      }
    });
  });
}

function getAvgRating(id) {
  return new Promise((resolve, reject) => {
    reviewsDB.getAverageRating(id, function (error, result) {
      if (error) {
        reject(error);
      } else {
        resolve(result[0]);
      }
    });
  });
}

module.exports = { getAllRestaurants };
