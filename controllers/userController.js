"use strict";
const UserDB = require("../models/UserDB");
const User = require("../models/Users");
const bcrypt = require("bcrypt");
var jwt = require("jsonwebtoken");
const res = require("express/lib/response");
// var secret = "somesecretkey";
var userDB = new UserDB();
function getAllUser(request, respond) {
  userDB.getAllUser(function (error, result) {
    if (error) {
      respond.json(error);
    } else {
      respond.json(result);
    }
  });
}
module.exports = { getAllUser };
async function addUser(request, respond) {
  var username = request.body.user_Uname;
  var password = request.body.user_passW;
  //   password = await bcrypt.hash(password, 10);
  var firstname = request.body.user_fName;
  var lastname = request.body.user_lName;
  var gender = request.body.user_gender;
  var age = request.body.user_age;
  var address = request.body.user_address;
  var postalcode = request.body.user_postalCode;
  var phoneno = request.body.user_phoneNo;
  var email = request.body.user_email;
  var userimage = request.body.user_img;
  console.log(request.body);
  /*var user = new User(null,
                            request.body.user_Uname,
                            request.body.user_passW,
                            user_passW = bcrypt.hashSync(user_passW),
                            request.body.user_fName,
                            request.body.user_lName,
                            request.body.user_gender,
                            request.body.user_age,
                            request.body.user_address,
                            request.body.user_postalCode,
                            request.body.user_phoneNo,
                            request.body.user_email,
                            request.body.user_img);*/
  userDB.addUser(
    username,
    password,
    firstname,
    lastname,
    gender,
    age,
    address,
    postalcode,
    phoneno,
    email,
    userimage,
    function (error, result) {
      if (error) {
        respond.json(error);
      } else {
        respond.json(result);
      }
    }
  );
}
function loginUser(request, respond) {
  var username = request.body.user_Uname;
  var password = request.body.user_passW;
  userDB.loginUser(username, function (error, result) {
    if (error) {
      respond.json(error);
    } else {
      /*const json_string = JSON.stringify(result);
            const json_pass = JSON.parse(json_string);
            // console.log(`The password send from database is [${json_pass[0].user_passW}]`);*/
      if (result.length > 0) {
        console.log("result", result);
        const hash = result[0].user_passW;
        const userId = result[0]._idUser;
        // const userId = result[1]._idUser;
        var flag = hash == password;
        //   var flag = bcrypt.compareSync(password, hash);
        if (flag) {
          // var token = jwt.sign(username, secret);
          respond.json({ username: username, userId: userId });
        } else {
          respond.json({ result: "invalid Username or Password" });
        }
      } else {
        respond.json({ result: "invalid" });
      }
    }
  });
}
function updateUser(request, respond) {
  var username = request.body.user_Uname;
  var firstname = request.body.user_fName;
  var lastname = request.body.user_lName;
  var gender = request.body.user_gender;
  var age = request.body.user_age;
  var address = request.body.user_address;
  var postalcode = request.body.user_postalCode;
  var phoneno = request.body.user_phoneNo;
  var email = request.body.user_email;
  var userimage = request.body.user_img;
  var token = request.body.token;
  try {
    var decoded = jwt.verify(token, secret);
    userDB.updateUser(
      username,
      firstname,
      lastname,
      gender,
      age,
      address,
      postalcode,
      phoneno,
      email,
      userimage,
      function (error, result) {
        if (error) {
          respond.json(error);
        } else {
          respond.json(result);
        }
      }
    );
  } catch (error) {
    respond.json({ result: "invalid token" });
  }
  /*userDB.updateUser(username,password,firstname, lastname, gender, age, address, postalcode, phoneno, email, userimage, function(error, result){
        if(error){
            respond.json(error);
        }
        else{
            respond.json(result);
        }
    });*/
}
function deleteUser(request, respond) {
  var _idUser = request.params._idUser;
  userDB.deleteUser(_idUser, function (error, result) {
    if (error) {
      respond.json(error);
    } else {
      respond.json(result);
    }
  });
}
function getSuggestedAddress(request, respond) {
  var user_postalCode = request.params.user_postalCode;
  userDB.getSuggestedAddress(user_postalCode, function (error, result) {
    if (error) {
      respond.json(error);
    } else {
      respond.json(result);
    }
  });
}
module.exports = {
  getAllUser,
  loginUser,
  addUser,
  updateUser,
  deleteUser,
  getSuggestedAddress,
};
