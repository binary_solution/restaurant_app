"use strict";
const FavouriteDB = require('../models/FavouriteDB');
const Favourite = require('../models/FavouriteDB');

var favouriteDB = new FavouriteDB();

function getAllFavourites(request, respond){
    favouriteDB.getAllFavourites(function(error, result){
        if(error){
            respond.json(error);
        }
        else{
            respond.json(result);
        }
    });

}

function getFavbyID(request,respond){
    var user_idF = request.params.user_idF;
    favouriteDB.getFavbyID(user_idF, function(error, result){
        if(error){
            respond.json(error);
        }
        else{
            respond.json(result);
        }
    });
}

module.exports = {getAllFavourites, getFavbyID};