"use strict";
const ReviewsDB = require('../models/ReviewsDB');
const Review = require('../models/Review');

var reviewsDB = new ReviewsDB();

function getAllReviews(request, respond){
    reviewsDB.getAllReviews(function(error, result){
        if(error){
            respond.json(error);
        }
        else{
            respond.json(result);
        }
    });

}

module.exports = {getAllReviews};

function addReview(request, respond){
    var now = new Date();
    var review = new Review(null,
                            request.body.restaurant_id,
                            request.body.user_id,
                            request.body.rev_restName,
                            request.body.review,
                            request.body.rev_rating,
                            now.toString());

    reviewsDB.addReview(review, function(error, result)
    {
        if(error){
            respond.json(error);
        }
        else{
            respond.json(result);
        }
    })
}


function updateReview(request, respond){
    var now = new Date();
    var review = new Review(request.params._idReviews,
                                    request.body.restaurant_id,
                                    request.body.user_id,
                                    request.body.rev_restName,
                                    request.body.review,
                                    request.body.rev_rating,
                                    now.toString());
    reviewsDB.updateReview(review, function(error, result){
        if(error){
            respond.json(error);
        }
        else{
            respond.json(result);
        }
    });
}

function deleteReview(request, respond){
    var _idReviews = request.params._idReviews;
    reviewsDB.deleteReview(_idReviews, function(error, result){
        if(error){
            respond.json(error);
        }
        else{
            respond.json(result);
        }
    });
}

function getReviewCountIndivRestaurant(request,respond){
    var restaurant_id = request.params.restaurant_id;
    reviewsDB.getReviewCountIndivRestaurant(restaurant_id, function(error, result){
        if(error){
            respond.json(error);
        }
        else{
            respond.json(result);
        }
    });
}

function getAverageRating(request,respond){
    var restaurant_id = request.params.restaurant_id;
    reviewsDB.getAverageRating(restaurant_id, function(error, result){
        if(error){
            respond.json(error);
        }
        else{
            respond.json(result);
        }
    });
}


function getReviewsbyUser(request,respond){
    var user_id = request.params.user_id;
    reviewsDB.getReviewsbyUser(user_id, function(error, result){
        if(error){
            respond.json(error);
        }
        else{
            respond.json(result);
        }
    });
}


module.exports = {getAllReviews, addReview, updateReview, deleteReview, getReviewCountIndivRestaurant, getAverageRating, getReviewsbyUser};
